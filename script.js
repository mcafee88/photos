const image = document.getElementById('image')

navigator.geolocation.getCurrentPosition(function(position) {
    console.log(position.coords.latitude, position.coords.longitude);
});


let queryUrl = "https://flickr.com/services/rest/?api_key=e4f0982226706f2976c7d6f17c93e264&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=39.8388686&lon=-86.27301399999999&text=dog"


function theFetch() {
    fetch(queryUrl)
        .then(data => data.json())
        .then(photoObj => {
            console.log(photoObj)
            console.log(constructImageURL(photoObj))
            image.src = constructImageURL(photoObj)
        })
}

const key = "f7521523e04c7664e24df40b2c724a0a"

function constructImageURL(photoObj) {
    let number = picNum()
    return "https://farm" + photoObj.photos.photo[number].farm +
        ".staticflickr.com/" + photoObj.photos.photo[number].server +
        "/" + photoObj.photos.photo[number].id + "_" + photoObj.photos.photo[number].secret + ".jpg";
}
theFetch()

const button = document.getElementById("next")

let picNumber = 0

function picNum() {
    if (picNumber == 5) {
        picNumber = 0
    } else {
        picNumber++
    }
    return picNumber
}

button.onclick = function() {
    theFetch()
}